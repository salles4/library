import supabase from "./supabase.js";

let queryString = window.location.search;
let queryParams = new URLSearchParams(queryString.substring(1));
let BOOKID
if (queryParams.has("id")) {
    BOOKID = decodeURIComponent(queryParams.get("id"));
}

// SELECT *
// FROM author 
// JOIN author a ON books.author_id = a.id 
// JOIN publisher p ON books.id = p.id
// WHERE books.id = ${BOOKID};

supabase
    .from("author")
    .select()
    .eq("id", BOOKID)
    .then(response => {
        console.log(response)
        if (response.error) {
            throw response.error;
        }
        let row = response.data[0]

        $("#img-cover").attr("src", `img/default-profile.jpg`)
        $("#author-name").html(row.name)
        $("#author-bio").html(row.bio)

        $("#author-website").html(row.website)
        $("#author-website").attr("href", `author.html?id=${row.website}`)

        $("#loading").remove()
    }
    )
    .catch(error => {
        $("#loading").html(`
            <div class="d-flex align-items-center justify-content-center" style="height:80vh;">
                <div class="text-center">
                    <h2>Error</h2>
                    <p>${error.code}: ${error.message}</p>
                    <a href="index.html" class="fw-bold">Return Home</a> 
                </div>
            </div>
        `)
    }
    )